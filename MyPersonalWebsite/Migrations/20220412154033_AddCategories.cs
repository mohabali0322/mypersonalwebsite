﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MyPersonalWebsite.Migrations
{
    public partial class AddCategories : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Categories",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CategoryName = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BlogCategory",
                columns: table => new
                {
                    BlogsId = table.Column<int>(type: "int", nullable: false),
                    CategoriesId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BlogCategory", x => new { x.BlogsId, x.CategoriesId });
                    table.ForeignKey(
                        name: "FK_BlogCategory_Blog_BlogsId",
                        column: x => x.BlogsId,
                        principalTable: "Blog",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BlogCategory_Categories_CategoriesId",
                        column: x => x.CategoriesId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.UpdateData(
                table: "Blog",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "CreatedOn", "UpdateOn" },
                values: new object[] { 2, new DateTime(2022, 4, 12, 16, 40, 32, 944, DateTimeKind.Local).AddTicks(2145), new DateTime(2022, 4, 12, 16, 40, 32, 944, DateTimeKind.Local).AddTicks(2175) });

            migrationBuilder.UpdateData(
                table: "Blog",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "CreatedOn", "UpdateOn" },
                values: new object[] { 1, new DateTime(2022, 4, 12, 16, 40, 32, 944, DateTimeKind.Local).AddTicks(2178), new DateTime(2022, 4, 12, 16, 40, 32, 944, DateTimeKind.Local).AddTicks(2179) });

            migrationBuilder.CreateIndex(
                name: "IX_BlogCategory_CategoriesId",
                table: "BlogCategory",
                column: "CategoriesId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BlogCategory");

            migrationBuilder.DropTable(
                name: "Categories");

            migrationBuilder.UpdateData(
                table: "Blog",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "CreatedOn", "UpdateOn" },
                values: new object[] { null, new DateTime(2022, 4, 11, 10, 3, 50, 667, DateTimeKind.Local).AddTicks(2444), new DateTime(2022, 4, 11, 10, 3, 50, 667, DateTimeKind.Local).AddTicks(2478) });

            migrationBuilder.UpdateData(
                table: "Blog",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "CreatedOn", "UpdateOn" },
                values: new object[] { null, new DateTime(2022, 4, 11, 10, 3, 50, 667, DateTimeKind.Local).AddTicks(2481), new DateTime(2022, 4, 11, 10, 3, 50, 667, DateTimeKind.Local).AddTicks(2482) });
        }
    }
}
