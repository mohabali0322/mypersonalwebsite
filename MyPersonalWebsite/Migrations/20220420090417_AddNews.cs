﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MyPersonalWebsite.Migrations
{
    public partial class AddNews : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "CategoryName",
                table: "Categories",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.CreateTable(
                name: "NewsCategories",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NewsCategories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "News",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    Body = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    PublishOn = table.Column<DateTime>(type: "datetime2", nullable: false),
                    PrimaryCategoryId = table.Column<int>(type: "int", nullable: true),
                    AuthorId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_News", x => x.Id);
                    table.ForeignKey(
                        name: "FK_News_Authors_AuthorId",
                        column: x => x.AuthorId,
                        principalTable: "Authors",
                        principalColumn: "AuthorId");
                    table.ForeignKey(
                        name: "FK_News_NewsCategories_PrimaryCategoryId",
                        column: x => x.PrimaryCategoryId,
                        principalTable: "NewsCategories",
                        principalColumn: "Id");
                });

            migrationBuilder.UpdateData(
                table: "Blog",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedOn", "UpdateOn" },
                values: new object[] { new DateTime(2022, 4, 20, 10, 4, 17, 333, DateTimeKind.Local).AddTicks(1826), new DateTime(2022, 4, 20, 10, 4, 17, 333, DateTimeKind.Local).AddTicks(1857) });

            migrationBuilder.UpdateData(
                table: "Blog",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedOn", "UpdateOn" },
                values: new object[] { new DateTime(2022, 4, 20, 10, 4, 17, 333, DateTimeKind.Local).AddTicks(1861), new DateTime(2022, 4, 20, 10, 4, 17, 333, DateTimeKind.Local).AddTicks(1863) });

            migrationBuilder.CreateIndex(
                name: "IX_News_AuthorId",
                table: "News",
                column: "AuthorId");

            migrationBuilder.CreateIndex(
                name: "IX_News_PrimaryCategoryId",
                table: "News",
                column: "PrimaryCategoryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "News");

            migrationBuilder.DropTable(
                name: "NewsCategories");

            migrationBuilder.AlterColumn<string>(
                name: "CategoryName",
                table: "Categories",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(255)",
                oldMaxLength: 255);

            migrationBuilder.UpdateData(
                table: "Blog",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedOn", "UpdateOn" },
                values: new object[] { new DateTime(2022, 4, 12, 16, 40, 32, 944, DateTimeKind.Local).AddTicks(2145), new DateTime(2022, 4, 12, 16, 40, 32, 944, DateTimeKind.Local).AddTicks(2175) });

            migrationBuilder.UpdateData(
                table: "Blog",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedOn", "UpdateOn" },
                values: new object[] { new DateTime(2022, 4, 12, 16, 40, 32, 944, DateTimeKind.Local).AddTicks(2178), new DateTime(2022, 4, 12, 16, 40, 32, 944, DateTimeKind.Local).AddTicks(2179) });
        }
    }
}
