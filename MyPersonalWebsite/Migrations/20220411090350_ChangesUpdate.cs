﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MyPersonalWebsite.Migrations
{
    public partial class ChangesUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Authors",
                newName: "AuthorId");

            migrationBuilder.AddColumn<int>(
                name: "AuthorId",
                table: "Blog",
                type: "int",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Blog",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedOn", "UpdateOn" },
                values: new object[] { new DateTime(2022, 4, 11, 10, 3, 50, 667, DateTimeKind.Local).AddTicks(2444), new DateTime(2022, 4, 11, 10, 3, 50, 667, DateTimeKind.Local).AddTicks(2478) });

            migrationBuilder.UpdateData(
                table: "Blog",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedOn", "UpdateOn" },
                values: new object[] { new DateTime(2022, 4, 11, 10, 3, 50, 667, DateTimeKind.Local).AddTicks(2481), new DateTime(2022, 4, 11, 10, 3, 50, 667, DateTimeKind.Local).AddTicks(2482) });

            migrationBuilder.CreateIndex(
                name: "IX_Blog_AuthorId",
                table: "Blog",
                column: "AuthorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Blog_Authors_AuthorId",
                table: "Blog",
                column: "AuthorId",
                principalTable: "Authors",
                principalColumn: "AuthorId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Blog_Authors_AuthorId",
                table: "Blog");

            migrationBuilder.DropIndex(
                name: "IX_Blog_AuthorId",
                table: "Blog");

            migrationBuilder.DropColumn(
                name: "AuthorId",
                table: "Blog");

            migrationBuilder.RenameColumn(
                name: "AuthorId",
                table: "Authors",
                newName: "Id");

            migrationBuilder.UpdateData(
                table: "Blog",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedOn", "UpdateOn" },
                values: new object[] { new DateTime(2022, 4, 7, 12, 7, 12, 631, DateTimeKind.Local).AddTicks(8995), new DateTime(2022, 4, 7, 12, 7, 12, 631, DateTimeKind.Local).AddTicks(9029) });

            migrationBuilder.UpdateData(
                table: "Blog",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedOn", "UpdateOn" },
                values: new object[] { new DateTime(2022, 4, 7, 12, 7, 12, 631, DateTimeKind.Local).AddTicks(9033), new DateTime(2022, 4, 7, 12, 7, 12, 631, DateTimeKind.Local).AddTicks(9035) });
        }
    }
}
