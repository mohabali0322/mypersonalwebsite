﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MyPersonalWebsite.Migrations
{
    public partial class ChangeHasData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Blog",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedOn", "UpdateOn" },
                values: new object[] { new DateTime(2022, 4, 21, 15, 26, 51, 659, DateTimeKind.Local).AddTicks(1140), new DateTime(2022, 4, 21, 15, 26, 51, 659, DateTimeKind.Local).AddTicks(1200) });

            migrationBuilder.UpdateData(
                table: "Blog",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedOn", "UpdateOn" },
                values: new object[] { new DateTime(2022, 4, 21, 15, 26, 51, 659, DateTimeKind.Local).AddTicks(1200), new DateTime(2022, 4, 21, 15, 26, 51, 659, DateTimeKind.Local).AddTicks(1200) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Blog",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedOn", "UpdateOn" },
                values: new object[] { new DateTime(2022, 4, 21, 15, 25, 31, 385, DateTimeKind.Local).AddTicks(5270), new DateTime(2022, 4, 21, 15, 25, 31, 385, DateTimeKind.Local).AddTicks(5310) });

            migrationBuilder.UpdateData(
                table: "Blog",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedOn", "UpdateOn" },
                values: new object[] { new DateTime(2022, 4, 21, 15, 25, 31, 385, DateTimeKind.Local).AddTicks(5320), new DateTime(2022, 4, 21, 15, 25, 31, 385, DateTimeKind.Local).AddTicks(5320) });
        }
    }
}
