﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MyPersonalWebsite.Migrations
{
    public partial class InitBlogData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Blog",
                columns: new[] { "Id", "Body", "CreatedOn", "Name", "UpdateOn" },
                values: new object[] { 1, "Some Body", new DateTime(2022, 3, 21, 12, 5, 11, 456, DateTimeKind.Local).AddTicks(6707), "Blog 1", new DateTime(2022, 3, 21, 12, 5, 11, 456, DateTimeKind.Local).AddTicks(6740) });

            migrationBuilder.InsertData(
                table: "Blog",
                columns: new[] { "Id", "Body", "CreatedOn", "Name", "UpdateOn" },
                values: new object[] { 2, "Some Body 2", new DateTime(2022, 3, 21, 12, 5, 11, 456, DateTimeKind.Local).AddTicks(6743), "Blog 2", new DateTime(2022, 3, 21, 12, 5, 11, 456, DateTimeKind.Local).AddTicks(6744) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Blog",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Blog",
                keyColumn: "Id",
                keyValue: 2);
        }
    }
}
