﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MyPersonalWebsite.Migrations
{
    public partial class AddManyToManyMappingsForNews : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "NewsNewsCategory",
                columns: table => new
                {
                    OtherCategoriesId = table.Column<int>(type: "int", nullable: false),
                    OtherCategoryNewsId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NewsNewsCategory", x => new { x.OtherCategoriesId, x.OtherCategoryNewsId });
                    table.ForeignKey(
                        name: "FK_NewsNewsCategory_News_OtherCategoryNewsId",
                        column: x => x.OtherCategoryNewsId,
                        principalTable: "News",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_NewsNewsCategory_NewsCategories_OtherCategoriesId",
                        column: x => x.OtherCategoriesId,
                        principalTable: "NewsCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);   
                });

            migrationBuilder.UpdateData(
                table: "Blog",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedOn", "UpdateOn" },
                values: new object[] { new DateTime(2022, 4, 21, 15, 25, 31, 385, DateTimeKind.Local).AddTicks(5270), new DateTime(2022, 4, 21, 15, 25, 31, 385, DateTimeKind.Local).AddTicks(5310) });

            migrationBuilder.UpdateData(
                table: "Blog",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedOn", "UpdateOn" },
                values: new object[] { new DateTime(2022, 4, 21, 15, 25, 31, 385, DateTimeKind.Local).AddTicks(5320), new DateTime(2022, 4, 21, 15, 25, 31, 385, DateTimeKind.Local).AddTicks(5320) });

            migrationBuilder.CreateIndex(
                name: "IX_NewsNewsCategory_OtherCategoryNewsId",
                table: "NewsNewsCategory",
                column: "OtherCategoryNewsId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "NewsNewsCategory");

            migrationBuilder.UpdateData(
                table: "Blog",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedOn", "UpdateOn" },
                values: new object[] { new DateTime(2022, 4, 21, 15, 15, 19, 306, DateTimeKind.Local).AddTicks(480), new DateTime(2022, 4, 21, 15, 15, 19, 306, DateTimeKind.Local).AddTicks(510) });

            migrationBuilder.UpdateData(
                table: "Blog",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedOn", "UpdateOn" },
                values: new object[] { new DateTime(2022, 4, 21, 15, 15, 19, 306, DateTimeKind.Local).AddTicks(520), new DateTime(2022, 4, 21, 15, 15, 19, 306, DateTimeKind.Local).AddTicks(520) });
        }
    }
}
