﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MyPersonalWebsite.Migrations
{
    public partial class AddComments : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Comments",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Body = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    BlogId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Comments_Blog_BlogId",
                        column: x => x.BlogId,
                        principalTable: "Blog",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.UpdateData(
                table: "Blog",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedOn", "UpdateOn" },
                values: new object[] { new DateTime(2022, 3, 31, 13, 53, 45, 72, DateTimeKind.Local).AddTicks(5093), new DateTime(2022, 3, 31, 13, 53, 45, 72, DateTimeKind.Local).AddTicks(5124) });

            migrationBuilder.UpdateData(
                table: "Blog",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedOn", "UpdateOn" },
                values: new object[] { new DateTime(2022, 3, 31, 13, 53, 45, 72, DateTimeKind.Local).AddTicks(5127), new DateTime(2022, 3, 31, 13, 53, 45, 72, DateTimeKind.Local).AddTicks(5129) });

            migrationBuilder.CreateIndex(
                name: "IX_Comments_BlogId",
                table: "Comments",
                column: "BlogId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Comments");

            migrationBuilder.UpdateData(
                table: "Blog",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedOn", "UpdateOn" },
                values: new object[] { new DateTime(2022, 3, 22, 17, 0, 45, 154, DateTimeKind.Local).AddTicks(4489), new DateTime(2022, 3, 22, 17, 0, 45, 154, DateTimeKind.Local).AddTicks(4524) });

            migrationBuilder.UpdateData(
                table: "Blog",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedOn", "UpdateOn" },
                values: new object[] { new DateTime(2022, 3, 22, 17, 0, 45, 154, DateTimeKind.Local).AddTicks(4528), new DateTime(2022, 3, 22, 17, 0, 45, 154, DateTimeKind.Local).AddTicks(4529) });
        }
    }
}
