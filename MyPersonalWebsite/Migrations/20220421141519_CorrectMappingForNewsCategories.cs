﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MyPersonalWebsite.Migrations
{
    public partial class CorrectMappingForNewsCategories : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "NewsNewsCategory");

            migrationBuilder.UpdateData(
                table: "Blog",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedOn", "UpdateOn" },
                values: new object[] { new DateTime(2022, 4, 21, 15, 15, 19, 306, DateTimeKind.Local).AddTicks(480), new DateTime(2022, 4, 21, 15, 15, 19, 306, DateTimeKind.Local).AddTicks(510) });

            migrationBuilder.UpdateData(
                table: "Blog",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedOn", "UpdateOn" },
                values: new object[] { new DateTime(2022, 4, 21, 15, 15, 19, 306, DateTimeKind.Local).AddTicks(520), new DateTime(2022, 4, 21, 15, 15, 19, 306, DateTimeKind.Local).AddTicks(520) });

            migrationBuilder.CreateIndex(
                name: "IX_News_PrimaryCategoryId",
                table: "News",
                column: "PrimaryCategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_News_NewsCategories_PrimaryCategoryId",
                table: "News",
                column: "PrimaryCategoryId",
                principalTable: "NewsCategories",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_News_NewsCategories_PrimaryCategoryId",
                table: "News");

            migrationBuilder.DropIndex(
                name: "IX_News_PrimaryCategoryId",
                table: "News");

            migrationBuilder.CreateTable(
                name: "NewsNewsCategory",
                columns: table => new
                {
                    NewsId = table.Column<int>(type: "int", nullable: false),
                    OtherCategoriesId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NewsNewsCategory", x => new { x.NewsId, x.OtherCategoriesId });
                    table.ForeignKey(
                        name: "FK_NewsNewsCategory_News_NewsId",
                        column: x => x.NewsId,
                        principalTable: "News",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_NewsNewsCategory_NewsCategories_OtherCategoriesId",
                        column: x => x.OtherCategoriesId,
                        principalTable: "NewsCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.UpdateData(
                table: "Blog",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedOn", "UpdateOn" },
                values: new object[] { new DateTime(2022, 4, 21, 14, 42, 24, 547, DateTimeKind.Local).AddTicks(1689), new DateTime(2022, 4, 21, 14, 42, 24, 547, DateTimeKind.Local).AddTicks(1724) });

            migrationBuilder.UpdateData(
                table: "Blog",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedOn", "UpdateOn" },
                values: new object[] { new DateTime(2022, 4, 21, 14, 42, 24, 547, DateTimeKind.Local).AddTicks(1728), new DateTime(2022, 4, 21, 14, 42, 24, 547, DateTimeKind.Local).AddTicks(1729) });

            migrationBuilder.CreateIndex(
                name: "IX_NewsNewsCategory_OtherCategoriesId",
                table: "NewsNewsCategory",
                column: "OtherCategoriesId");
        }
    }
}
