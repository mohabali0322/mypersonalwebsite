﻿using Microsoft.AspNetCore.Mvc;
using MyPersonalWebsite.ClassLib.Data;
using MyPersonalWebsite.ClassLib.Data.Entities;
using X.PagedList;
using MyPersonalWebsite.ClassLib.Models;
using MyPersonalWebsite.ClassLib.Services;
using MyPersonalWebsite.ClassLib.Services.IServices;

namespace MyPersonalWebsite.Controllers
{
    public class BlogController : Controller
    {
        private readonly IBlogUIService _service;

        public BlogController(IBlogUIService service)
        {
            _service = service;
        }

        public ViewResult Index(int? page)
        {
            IEnumerable<Blog> objBlogList = _service.GetAllBlogs();

            int pageSize = 3;
            int pageNumber = (page ?? 1);
            return View(objBlogList.ToPagedList(pageNumber, pageSize));
        }

        public IActionResult BlogDetail(int id)
        {
            Blog blogObj = _service.GetBlogById(id);

            return View(blogObj);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddComment(AddCommentModel addCommentModel)
        {
            if (ModelState.IsValid)
            {
                await _service.AddComment(addCommentModel);

                return RedirectToAction("BlogDetail", new { Id = addCommentModel.BlogId });
            }
            return View(addCommentModel);
        }
    }
}


