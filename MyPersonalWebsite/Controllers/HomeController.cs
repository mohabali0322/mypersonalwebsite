﻿using Microsoft.AspNetCore.Mvc;
using MyPersonalWebsite.ClassLib.Models;
using MyPersonalWebsite.ClassLib.Services;
using MyPersonalWebsite.ClassLib.Services.IServices;
using System.Diagnostics;

namespace MyPersonalWebsite.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IHomeUIService _homeService;

        public HomeController(ILogger<HomeController> logger, IHomeUIService homeService)
        {
            _logger = logger;
            _homeService = homeService;
        }

        public IActionResult Index()
        {
            var homePage = _homeService.HomePageDetails();
            return View(homePage);
        }


    }
}