﻿using Microsoft.AspNetCore.Mvc;
using MyPersonalWebsite.ClassLib.Models;
using MyPersonalWebsite.ClassLib.Services;
using MyPersonalWebsite.ClassLib.Services.IServices;

namespace MyPersonalWebsite.Controllers
{
    public class ContactController : Controller
    {
        private readonly IMailService _mailService;

        public ContactController(IMailService mailService)
        {
            _mailService = mailService;

        }

        public ActionResult Index()
        {
            ViewData["Success"] = TempData["Success"];
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]

        public ActionResult Index(ContactFormViewModel contactFormViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _mailService.Send(new MailMessageModel
                    {
                        ToAddress = "mohab.ali@thought.co.uk",
                        IsHtml = true,
                        Subject = "New Contact Us Request",
                        Body = contactFormViewModel.Message
                    });

                    TempData["Success"] = true;
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = "Some Error" + ex.Message;
            }
            return View();
        }
    }
}

