﻿using Microsoft.AspNetCore.Mvc;
using MyPersonalWebsite.ClassLib.Data.Entities;
using MyPersonalWebsite.ClassLib.Services.IServices;
using X.PagedList;

namespace MyPersonalWebsite.Controllers
{
    public class NewsController : Controller
    {
        private readonly INewsService _service;

        public NewsController(INewsService service)
        {
            _service = service;
        }
        public async Task<IActionResult> Index(int? page)
        {
            IEnumerable<News> objNewsList = await _service.GetAllNews();

            int pageSize = 10;
            int pageNumber = (page ?? 1);

            return View(objNewsList.ToPagedList(pageNumber, pageSize));
        }
    }
}
