﻿using Microsoft.AspNetCore.Mvc;
using MyPersonalWebsite.ClassLib.Data.Entities;
using MyPersonalWebsite.ClassLib.Services.IServices;
using X.PagedList;

namespace MyPersonalWebsite.Controllers
{
    public class NewsCategoryController : Controller
    {
       private readonly INewsCategoryService _service;

        public NewsCategoryController(INewsCategoryService service)
        {
            _service = service;
        }
        public async Task<IActionResult> Index(int? page)
        {
            IEnumerable<NewsCategory> objNewsCategoryList = await _service.GetAllNewsCategories();

            int pageSize = 10;
            int pageNumber = (page ?? 1);

            return View(objNewsCategoryList.ToPagedList(pageNumber, pageSize));
        }

        public async Task<IActionResult> Details(int id, int? page)
        {
            var pageNumber = page ?? 1;   
            ViewData["news"] = await _service.GetNewsforCategory(id);

            return View();
        }

    }
}
