﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MyPersonalWebsite.ClassLib.Data.Entities;
using MyPersonalWebsite.ClassLib.Models.Category;
using MyPersonalWebsite.ClassLib.Services.IServices;
using X.PagedList;

namespace MyPersonalWebsite.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    public class CategoryController : Controller
    {
        private readonly ICategoryService _service;

        public CategoryController(ICategoryService service)
        {
            _service = service;
        }
        public async Task<IActionResult> Index(int? page)
        {
            IEnumerable<Category> categoryList = await _service.GetAllCategoriesAsync();

            int pageSize = 10;
            int pageNumber = (page ?? 1);

            return View(categoryList.ToPagedList(pageNumber, pageSize));
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(AddCategoryModel category)
        {
            if (ModelState.IsValid)
            {
                await _service.AddCategoryAsync(category);

                return RedirectToAction("Index");
            }
            return View(category);
        }

        public async Task<IActionResult> EditAsync(int id)
        {
            if (id == null || id == 0)
            {
                return NotFound();
            }
            var categoryFromDb = await _service.GetCategoryByIdAsync(id);

            if (categoryFromDb == null)
            {
                return NotFound();
            }
            return View(categoryFromDb);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(EditCategoryModel category)
        {

            if (ModelState.IsValid)
            {
                await _service.UpdateCategoryAsync(category);
                return RedirectToAction("Index");
            }

            return View(category);

        }

        public async Task<IActionResult> DeleteAsync(int id)
        {
            if (id == null || id == 0)
            {
                return NotFound();
            }

            var categoryFromDb = await _service.GetCategoryDeleteByIdAsync(id);

            if (categoryFromDb == null)
            {
                return NotFound();
            }
            return View(categoryFromDb);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id)
        {

            await _service.DeleteCategoryAsync(id);

            return RedirectToAction("Index");

        }

    }
}
