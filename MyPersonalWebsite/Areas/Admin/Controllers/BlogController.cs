﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using MyPersonalWebsite.ClassLib.Data;
using MyPersonalWebsite.ClassLib.Data.Entities;
using MyPersonalWebsite.ClassLib.Models;
using MyPersonalWebsite.ClassLib.Models.Blog;
using MyPersonalWebsite.ClassLib.Services;
using MyPersonalWebsite.ClassLib.Services.IServices;
using X.PagedList;

namespace MyPersonalWebsite.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    public class BlogController : Controller
    {
        private readonly IBlogService _service;
        private readonly IBlogTagService _blogTagService;
        private readonly IAuthorService _authorService;
        private readonly ICategoryService _categoryService;

        public BlogController(IBlogService service, IBlogTagService blogTagService, IAuthorService authorService, ICategoryService categoryService)
        {
            _service = service;
            _blogTagService = blogTagService;
            _authorService = authorService;
            _categoryService = categoryService;
        }

        public async Task<IActionResult> Index(int? page)
        {
            IEnumerable<Blog> objBlogList = await _service.GetAllBlogs();

            int pageSize = 3;
            int pageNumber = (page ?? 1);

            return View(objBlogList.ToPagedList(pageNumber, pageSize));
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(AddBlogModel addBlogModel)
        {

            if (ModelState.IsValid)
            {
                await _service.AddBlog(addBlogModel);

                return RedirectToAction("Index");
            }
            return View(addBlogModel);
        }

        public async Task<IActionResult> EditAsync(int id)
        {
            var blogFromDb = await _service.GetBlogByIdAsync(id);
            if (blogFromDb == null)
            {
                return NotFound();
            }

            ViewData["tags"] = await _blogTagService.GetTagsForBlog(id);
            ViewData["authors"]= await _authorService.GetAuthorOptions();
            ViewData["categories"]= await _categoryService.GetCategoryOptions();
            
            return View(blogFromDb);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditAsync(EditBlogModel obj)
        {
          

            if (ModelState.IsValid)
            {
                await _service.UpdateBlog(obj);
                return RedirectToAction("Index");
            }

            return View(obj);

        }

        public async Task<IActionResult> DeleteAsync(int id)
        {
            if (id == null || id == 0)
            {
                return NotFound();
            }

            var blogFromDb = await _service.GetBlogDeleteByIdAsync(id);

            if (blogFromDb == null)
            {
                return NotFound();
            }
            return View(blogFromDb);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeletePOST(int id)
        {

            await _service.DeleteBlog(id);

            return RedirectToAction("Index");

        }

    }
    
}





