﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MyPersonalWebsite.ClassLib.Data.Entities;
using MyPersonalWebsite.ClassLib.Models.NewsCategory;
using MyPersonalWebsite.ClassLib.Services.IServices;
using X.PagedList;

namespace MyPersonalWebsite.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    public class NewsCategoryController : Controller
    {
        private readonly INewsCategoryService _service;

        public NewsCategoryController(INewsCategoryService service)
        {
            _service = service;
        }
        public async Task<IActionResult> Index(int? page)
        {
            IEnumerable<NewsCategory> objNewsCategoryList = await _service.GetAllNewsCategories();

            int pageSize = 10;
            int pageNumber = (page ?? 1);

            return View(objNewsCategoryList.ToPagedList(pageNumber, pageSize));
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(AddNewsCategoryModel addNewsCategoryModel)
        {

            if (ModelState.IsValid)
            {
                await _service.AddNewsCategory(addNewsCategoryModel);

                return RedirectToAction("Index");
            }
            return View(addNewsCategoryModel);
        }

        public async Task<IActionResult> EditAsync(int id)
        {
            var newsCategoryFromDb = await _service.GetNewsCategoryByIdAsync(id);
            if (newsCategoryFromDb == null)
            {
                return NotFound();
            }

            return View(newsCategoryFromDb);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditAsync(EditNewsCategoryModel editNewsModel)
        {
            if (ModelState.IsValid)
            {
                await _service.UpdateNewsCategory(editNewsModel);
                return RedirectToAction("Index");
            }

            return View(editNewsModel);
        }

        public async Task<IActionResult> DeleteAsync(int id)
        {
            if (id == null || id == 0)
            {
                return NotFound();
            }

            var newsCategoryFromDb = await _service.GetNewsCategoryById(id);

            if (newsCategoryFromDb == null)
            {
                return NotFound();
            }
            return View(newsCategoryFromDb);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeletePOST(int id)
        {

            await _service.DeleteNewsCategory(id);

            return RedirectToAction("Index");

        }
    }
}
