﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MyPersonalWebsite.ClassLib.Data.Entities;
using MyPersonalWebsite.ClassLib.Models.Author;
using MyPersonalWebsite.ClassLib.Services.IServices;
using X.PagedList;

namespace MyPersonalWebsite.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    public class AuthorController : Controller
    {
        private readonly IAuthorService _service;

        public AuthorController(IAuthorService service)
        {
            _service = service;
        }

        public async Task<IActionResult> Index(int? page)
        {
            IEnumerable<Author> authorList = await _service.GetAllAuthorsAsync();

            int pageSize = 10;
            int pageNumber = (page ?? 1);

            return View(authorList.ToPagedList(pageNumber, pageSize));
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(AddAuthorModel author)
        {

            if (ModelState.IsValid)
            {
                await _service.AddAuthorAsync(author);

                return RedirectToAction("Index");
            }
            return View(author);
        }

        public async Task<IActionResult> EditAsync(int id)
        {
            if (id == null || id == 0)
            {
                return NotFound();
            }
            var authorFromDb = await _service.GetAuthorByIdAsync(id);

            if (authorFromDb == null)
            {
                return NotFound();
            }
            return View(authorFromDb);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(EditAuthorModel author)
        {

            if (ModelState.IsValid)
            {
                await _service.UpdateAuthorAsync(author);
                return RedirectToAction("Index");
            }

            return View(author);

        }

        public async Task<IActionResult> DeleteAsync(int id)
        {
            if (id == null || id == 0)
            {
                return NotFound();
            }

            var authorFromDb = await _service.GetAuthorDeleteByIdAsync(id);

            if (authorFromDb == null)
            {
                return NotFound();
            }
            return View(authorFromDb);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id)
        {

            await _service.DeleteAuthorAsync(id);

            return RedirectToAction("Index");

        }
    }
}
