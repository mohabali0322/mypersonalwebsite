﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MyPersonalWebsite.ClassLib.Data.Entities;
using MyPersonalWebsite.ClassLib.Models.News;
using MyPersonalWebsite.ClassLib.Services.IServices;
using X.PagedList;

namespace MyPersonalWebsite.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    public class NewsController : Controller
    {
        private readonly INewsService _service;
        private readonly IAuthorService _authorService;
        private readonly INewsCategoryService _categoryService;
        public NewsController(INewsService service, IAuthorService authorService, INewsCategoryService newsCategoryService)
        {
            _service = service;
            _authorService = authorService;
            _categoryService = newsCategoryService;
        }
        public async Task<IActionResult> Index(int? page)
        {
            IEnumerable<News> objNewsList = await _service.GetAllNews();

            int pageSize = 10;
            int pageNumber = (page ?? 1);

            return View(objNewsList.ToPagedList(pageNumber, pageSize));
        }

        public IActionResult Create()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(AddNewsModel addNewsModel)
        {

            if (ModelState.IsValid)
            {
                await _service.AddNews(addNewsModel);

                return RedirectToAction("Index");
            }
            return View(addNewsModel);
        }

        public async Task<IActionResult> EditAsync(int id)
        {
            var newsFromDb = await _service.GetNewsByIdAsync(id);
            if (newsFromDb == null)
            {
                return NotFound();
            }

            ViewData["authors"] = await _authorService.GetAuthorOptions();
            ViewData["categories"] = await _categoryService.GetCategoryOptions();
            ViewData["otherCategories"] = await _categoryService.GetOtherCategoryOptions();

            return View(newsFromDb);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditAsync(EditNewsModel editNewsModel)
        {
            if (ModelState.IsValid)
            {
                await _service.UpdateNews(editNewsModel);
                return RedirectToAction("Index");
            }

            return View(editNewsModel);
        }

        public async Task<IActionResult> DeleteAsync(int id)
        {
            if (id == null || id == 0)
            {
                return NotFound();
            }

            var newsFromDb = await _service.GetNewsById(id);

            if (newsFromDb == null)
            {
                return NotFound();
            }
            return View(newsFromDb);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeletePOST(int id)
        {

            await _service.DeleteNews(id);

            return RedirectToAction("Index");

        }
    }
}
