﻿using Microsoft.AspNetCore.Mvc;
using MyPersonalWebsite.ClassLib.Models;
using MyPersonalWebsite.ClassLib.Services.IServices;

namespace MyPersonalWebsite.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Route("admin")]
    public class HomeController : Controller
    {
        private readonly IHomeService _homeService;


        public HomeController(IHomeService homeService)
        {
            _homeService = homeService;
        }


        public async Task<IActionResult> Index()
        {
            var homePage = await _homeService.HomePageDetails();
            return View(homePage);
        }

    }

}


