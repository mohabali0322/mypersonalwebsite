﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MyPersonalWebsite.ClassLib.Data.Entities;
using MyPersonalWebsite.ClassLib.Models;
using MyPersonalWebsite.ClassLib.Services.IServices;
using System.Linq;

namespace MyPersonalWebsite.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    public class TagController : Controller
    {
        private readonly IBlogTagService _service;

        public TagController(IBlogTagService service)
        {
            _service = service;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddTag(AddTagModel addTagModel)
        {
            if (ModelState.IsValid)
            {
                await _service.AddTag(addTagModel);

                return RedirectToAction("Edit", "Blog", new { Id = addTagModel.BlogsId });
            }
            return View(addTagModel);
        }

        [HttpGet]
        public async Task<IActionResult> DeleteTag(DeleteTagModel model)
        {
            return View(await _service.GetDeleteModel(model));
        }

        [HttpPost, ActionName("DeleteTag")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteTag_POST(DeleteTagModel deleteTagModel)
        {
            await _service.DeleteTag(deleteTagModel);

            return RedirectToAction("Edit", "Blog", new { Id = deleteTagModel.BlogId });
        }
    }
}


