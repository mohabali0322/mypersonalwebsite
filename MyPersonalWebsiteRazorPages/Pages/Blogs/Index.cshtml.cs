using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace MyPersonalWebsiteRazorPages.Pages.Blogs
{
    public class BlogModel : PageModel
    {

        public IList<BlogModel> blogs { get; set; }
        public string Name { get; private set; }
        public DateTime BlogTime { get; private set; }
        public string Message { get; private set; }
        public string Image { get; set; }

        public void OnGet()

        {
            blogs = new List<BlogModel>();
            {
                blogs.Add(new BlogModel { Name = "Barnsley�s Poya Asbaghi: �I still think about the people that died. It was just chance", BlogTime = DateTime.Parse("2005/09/01 07:22:16 AM"), Message = "oya Asbaghi was at Zurich airport waiting for a connecting flight to Stockholm from Barcelona when the then assistant manager of Dalkurd, a team founded by the Kurdish diaspora in the Swedish county of Dalarna, received a panicked call from the chairman. �He asked: �Where are you?�� says Asbaghi, now the head coach of Barnsley. �I told him I was in Switzerland and he said: �One of those planes from Barcelona has crashed.� I didn�t know which plane but, of course, you start doing the maths. I understood that there was a bigger chance that some of our players were on that flight than not. We didn�t get to speak more before we had to go on our next flight. We were sitting on that flight just thinking about which players might have died, basically.", Image = "/Images/EddeHow.png" });
                blogs.Add(new BlogModel { Name = "Barnsley�s Poya Asbaghi: �I still think about the people that died. It was just chance", BlogTime = DateTime.Parse("2009/10/20 10:20:16 AM"), Message = "oya Asbaghi was at Zurich airport waiting for a connecting flight to Stockholm from Barcelona when the then assistant manager of Dalkurd, a team founded by the Kurdish diaspora in the Swedish county of Dalarna, received a panicked call from the chairman. �He asked: �Where are you?�� says Asbaghi, now the head coach of Barnsley. �I told him I was in Switzerland and he said: �One of those planes from Barcelona has crashed.� I didn�t know which plane but, of course, you start doing the maths. I understood that there was a bigger chance that some of our players were on that flight than not. We didn�t get to speak more before we had to go on our next flight. We were sitting on that flight just thinking about which players might have died, basically.", Image = "/Images/Joelinton.png" });
                blogs.Add(new BlogModel { Name = "Barnsley�s Poya Asbaghi: �I still think about the people that died. It was just chance", BlogTime = DateTime.Parse("2012/02/15 09:30:16 AM"), Message = "oya Asbaghi was at Zurich airport waiting for a connecting flight to Stockholm from Barcelona when the then assistant manager of Dalkurd, a team founded by the Kurdish diaspora in the Swedish county of Dalarna, received a panicked call from the chairman. �He asked: �Where are you?�� says Asbaghi, now the head coach of Barnsley. �I told him I was in Switzerland and he said: �One of those planes from Barcelona has crashed.� I didn�t know which plane but, of course, you start doing the maths. I understood that there was a bigger chance that some of our players were on that flight than not. We didn�t get to speak more before we had to go on our next flight. We were sitting on that flight just thinking about which players might have died, basically.", Image = "/Images/Empty.png" });
                blogs.Add(new BlogModel { Name = "Barnsley�s Poya Asbaghi: �I still think about the people that died. It was just chance", BlogTime = DateTime.Parse("2005/09/01 11:15:00 AM"), Message = "oya Asbaghi was at Zurich airport waiting for a connecting flight to Stockholm from Barcelona when the then assistant manager of Dalkurd, a team founded by the Kurdish diaspora in the Swedish county of Dalarna, received a panicked call from the chairman. �He asked: �Where are you?�� says Asbaghi, now the head coach of Barnsley. �I told him I was in Switzerland and he said: �One of those planes from Barcelona has crashed.� I didn�t know which plane but, of course, you start doing the maths. I understood that there was a bigger chance that some of our players were on that flight than not. We didn�t get to speak more before we had to go on our next flight. We were sitting on that flight just thinking about which players might have died, basically.", Image = "/Images/Empty.png" });

            }
        }
    }
}
