using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.ComponentModel.DataAnnotations;
using MyPersonalWebsite.ClassLib.Models;
using System.Net.Mail;
using MyPersonalWebsite.ClassLib.Services;
using Microsoft.Extensions.Options;
using MyPersonalWebsite.ClassLib.Settings;

namespace MyPersonalWebsiteRazorPages.Pages.ContactForm
{
    public class ContactPageModel : PageModel
    {
        private readonly IMailService _mailService;

        public ContactPageModel(IMailService mailService)
        {
            _mailService = mailService;
        }

        [BindProperty]
        public ContactFormViewModel contactform { get; set; }

        public void OnGet()
        {
            contactform = new ContactFormViewModel();
        }

        public async Task OnPost()
        {
            if (ModelState.IsValid)
            {
                await _mailService.Send(new MailMessageModel
                {
                    ToAddress = "mohab.ali@thought.co.uk",
                    IsHtml = true,
                    Subject = "new Contact Us Request",
                    Body = contactform.Message
                });
            }
        }
    }
}





