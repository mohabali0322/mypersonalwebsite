﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using MyPersonalWebsite.ClassLib.Models;

namespace MyPersonalWebsitesRazorPages.Pages.HomePage
{
    public class IndexModel : PageModel

    {

        private readonly ILogger<IndexModel> _logger;

        public HomePageModel homePage { get; set; }

        public string HeroImageUrl { get; set; }
        public string HomePageText { get; set; }


        public IndexModel(ILogger<IndexModel> logger)
        {
            _logger = logger;
        }

        public void OnGet()

        {
            homePage = new HomePageModel();
            {
                HeroImageUrl = "/Images/Newcastle.png";

                HomePageText = "Rivals have forced Newcastle United to pay £22million more than the going rate for their four buys in the January transfer window, exploiting the Magpies' new wealth and desperation to strengthen their squad to avoid relegation. " +
                 "Owners all over Europe were on high alert when the transfer market reopened at the beginning of January, mindful Newcastle had become the richest football club in the world after the Saudi Arabia Public Investment Fund(PIF) became the majority shareholder ";
            }
        }
    };

}

    
