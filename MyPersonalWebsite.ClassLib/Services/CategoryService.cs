﻿using Microsoft.EntityFrameworkCore;
using MyPersonalWebsite.ClassLib.Data;
using MyPersonalWebsite.ClassLib.Data.Entities;
using MyPersonalWebsite.ClassLib.Models;
using MyPersonalWebsite.ClassLib.Models.Category;
using MyPersonalWebsite.ClassLib.Services.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPersonalWebsite.ClassLib.Services
{
    public class CategoryService : ICategoryService

    {
        private MyDbContext _db;

        public CategoryService(MyDbContext db)
        {
            _db = db;
        }

        public async Task<List<Category>> GetAllCategoriesAsync()
        {
            var categoryList = await _db.Categories.ToListAsync();

            return categoryList;
        }

        public async Task AddCategoryAsync(AddCategoryModel category)
        {
            var newCategory = new Category
            {
                CategoryName = category.Name,
            };
            _db.Add(newCategory);
            
            await _db.SaveChangesAsync();
        }
        public async Task UpdateCategoryAsync(EditCategoryModel obj)
        { 
            var category = await _db.Categories.SingleAsync(x => x.Id == obj.Id);

            category.CategoryName = obj.Name;

            _db.Categories.Update(category);
            await _db.SaveChangesAsync();
        }

        public async Task DeleteCategoryAsync(int id)
        {
            var category = await _db.Categories.FirstOrDefaultAsync(u => u.Id == id);
            if (category!= null)
            {
                _db.Remove(category);
                await _db.SaveChangesAsync();
            }
        }
        public async Task<EditCategoryModel> GetCategoryByIdAsync(int id)
        {
            var category = await _db.Categories.SingleAsync(u => u.Id == id);

            var categoryModel = new EditCategoryModel
            {
                Id = category.Id,
                Name = category.CategoryName
            };

            return categoryModel;
        }

        public async Task<IEnumerable<CategoryViewModel>> GetCategoryOptions()
        {
            var categories = await GetAllCategoriesAsync();

            return (from category in categories
                    select new CategoryViewModel()
                    {
                        Id = category.Id,
                        Name = category.CategoryName,
                        IsSelected = false
                    }).ToList();
        }

        public async Task<Category> GetCategoryDeleteByIdAsync(int id)
        {
          var deleteCategory = await _db.Categories.SingleAsync(u => u.Id == id);

          return deleteCategory;
        }
    }
}
