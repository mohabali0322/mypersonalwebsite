﻿using MyPersonalWebsite.ClassLib.Data;
using MyPersonalWebsite.ClassLib.Data.Entities;
using MyPersonalWebsite.ClassLib.Models;
using MyPersonalWebsite.ClassLib.Services.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MyPersonalWebsite.ClassLib.Services
{
    public class BlogUIService : IBlogUIService

    {
        private MyDbContext _db;

        public BlogUIService(MyDbContext db) 
        {
            _db = db;
        }

        public IEnumerable<Blog> GetAllBlogs()
        {
          var allBlogs = _db.Blogs.OrderByDescending(x => x.CreatedOn).ToList();
          return (allBlogs);
        
        }

        public Blog GetBlogById(int id)
        {
            Blog obj = _db.Blogs.FirstOrDefault(u => u.Id == id);

            return obj;
        }
        public async Task AddComment(AddCommentModel obj)
        {
            var dbComments = new Comment
            {
                Name = obj.Name,
                Body = obj.Body,
                BlogId = obj.BlogId,
            };

             _db.Comments.Add(dbComments);

            await _db.SaveChangesAsync();

        }



    }
}
