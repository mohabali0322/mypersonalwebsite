﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MyPersonalWebsite.ClassLib.Data;
using MyPersonalWebsite.ClassLib.Data.Entities;
using MyPersonalWebsite.ClassLib.Models.NewsCategory;
using MyPersonalWebsite.ClassLib.Services.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPersonalWebsite.ClassLib.Services
{
    public class NewsCategoryService : INewsCategoryService
    {
        private MyDbContext _db;

        public NewsCategoryService(MyDbContext db)
        {
            _db = db;
        }

        public async Task AddNewsCategory(AddNewsCategoryModel addNewsCategoryModel)
        {
            var dbNewsCategory = new NewsCategory
            {
                Name = addNewsCategoryModel.Name,
            };

            _db.NewsCategories.Add(dbNewsCategory);
            await _db.SaveChangesAsync();
        }

        public async Task DeleteNewsCategory(int id)
        {
            NewsCategory deleteNewsCategory = _db.NewsCategories.FirstOrDefault(u => u.Id == id);
            if (deleteNewsCategory != null)
            {
                _db.Remove(deleteNewsCategory);
                await _db.SaveChangesAsync();
            }
        }

        public async Task<List<NewsCategory>> GetAllNewsCategories()
        {
            var newsCategoryList = await _db.NewsCategories.ToListAsync();

            return newsCategoryList;
        }

        public async Task<NewsCategory> GetNewsCategoryById(int id)
        {
            NewsCategory newsCategory = await _db.NewsCategories.Include(x=>x.PrimaryCategoryNews).SingleAsync(u => u.Id == id);

            return newsCategory;
        }

        public async Task<EditNewsCategoryModel> GetNewsCategoryByIdAsync(int id)
        {
           var newsCategoryById = await _db.NewsCategories.SingleOrDefaultAsync(u => u.Id == id);
            var editNewsCategory = new EditNewsCategoryModel
            {
                Id = newsCategoryById.Id,
                Name = newsCategoryById.Name,
            };

            return editNewsCategory;
        }

        public async Task UpdateNewsCategory(EditNewsCategoryModel editNewsModel)
        {
            var newsCategory = await _db.NewsCategories.SingleAsync(x => x.Id == editNewsModel.Id);

            newsCategory.Name = editNewsModel.Name;

            _db.NewsCategories.Update(newsCategory);
            await _db.SaveChangesAsync();
        }

        public async Task<List<SelectListItem>> GetCategoryOptions()
        {
            var newsCategories = await GetAllNewsCategories();

            return (from newsCategory in newsCategories
                    select new SelectListItem()
                    {
                        Text = newsCategory.Name,
                        Value = newsCategory.Id.ToString(),
                    }).ToList();
        }

        public async Task<List<News>> GetNewsforCategory(int id)
        {
           return await _db.NewsCategories.Include(x => x.PrimaryCategoryNews).Where(x => x.Id == id).SelectMany(x => x.PrimaryCategoryNews).ToListAsync();
        }

        public async Task<IEnumerable<NewsCategoryViewModel>> GetOtherCategoryOptions()
        {
            var newsCategories = await GetAllNewsCategories();

            return (from newsCategory in newsCategories
                    select new NewsCategoryViewModel()
                    {
                        Id = newsCategory.Id,
                        Name = newsCategory.Name,
                        IsSelected = false
                    }).ToList();
        }
    }
}
