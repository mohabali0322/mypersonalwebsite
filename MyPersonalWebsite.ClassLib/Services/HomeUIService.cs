﻿using MyPersonalWebsite.ClassLib.Models;
using MyPersonalWebsite.ClassLib.Services.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPersonalWebsite.ClassLib.Services
{
   public class HomeUIService : IHomeUIService
    {
        public HomePageModel HomePageDetails()
        {

            var myHomeModel = new HomePageModel
            {
                HeroImageUrl = "Images/Newcastle.jpg",
                HomePageText = "Rivals have forced Newcastle United to pay £22million more than the going rate for their four buys in the January transfer window, exploiting the Magpies' new wealth and desperation to strengthen their squad to avoid relegation. " +
                 "Owners all over Europe were on high alert when the transfer market reopened at the beginning of January, mindful Newcastle had become the richest football club in the world after the Saudi Arabia Public Investment Fund(PIF) became the majority shareholder"
            };
            return myHomeModel;
        }
    }
}
