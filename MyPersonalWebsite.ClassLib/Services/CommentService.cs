﻿using Microsoft.EntityFrameworkCore;
using MyPersonalWebsite.ClassLib.Data;
using MyPersonalWebsite.ClassLib.Data.Entities;
using MyPersonalWebsite.ClassLib.Services.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPersonalWebsite.ClassLib.Services
{
    public class CommentService : ICommentService

    {
        private MyDbContext _db;

        public CommentService(MyDbContext db)
        {
            _db = db;
        }

        public async Task<List<Comment>> GetComments(int id)
        {
            List<Comment> commentlist = await _db.Comments.Where(x => x.BlogId == id).ToListAsync();

           return commentlist;
        }

    }
}

