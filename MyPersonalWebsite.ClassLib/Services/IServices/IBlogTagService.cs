﻿using MyPersonalWebsite.ClassLib.Data.Entities;
using MyPersonalWebsite.ClassLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPersonalWebsite.ClassLib.Services.IServices
{
    public interface IBlogTagService
    {
        Task<AddTagResponse> AddTag(AddTagModel addTagModel);

        Task<DeleteTagModel> GetDeleteModel(DeleteTagModel deleteTagModel);

        Task DeleteTag(DeleteTagModel model);
        Task<List<Tag>> GetTagsForBlog(int id);
    }
}
