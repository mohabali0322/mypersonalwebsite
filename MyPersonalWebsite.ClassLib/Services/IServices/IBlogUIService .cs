﻿using MyPersonalWebsite.ClassLib.Data.Entities;
using MyPersonalWebsite.ClassLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MyPersonalWebsite.ClassLib.Services.IServices
{
    public interface IBlogUIService
    {
        IEnumerable<Blog> GetAllBlogs();

        Blog GetBlogById(int id);

        Task AddComment(AddCommentModel obj);




    }
}
