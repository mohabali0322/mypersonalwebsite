﻿using MyPersonalWebsite.ClassLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPersonalWebsite.ClassLib.Services.IServices
{
    public interface IHomeUIService
    {
        HomePageModel HomePageDetails();
    }
}
