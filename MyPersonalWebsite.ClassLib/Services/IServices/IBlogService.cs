﻿using MyPersonalWebsite.ClassLib.Data.Entities;
using MyPersonalWebsite.ClassLib.Models;
using MyPersonalWebsite.ClassLib.Models.Blog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MyPersonalWebsite.ClassLib.Services.IServices
{
    public interface IBlogService
    {
        Task<List<Blog>> GetAllBlogs();

        Task AddBlog(AddBlogModel addBlogModel);

        Task DeleteBlog(int id);

        Task UpdateBlog(EditBlogModel obj);

        Task<EditBlogModel> GetBlogByIdAsync(int id);

        Task<Blog> GetBlogDeleteByIdAsync(int id);
    }
}
