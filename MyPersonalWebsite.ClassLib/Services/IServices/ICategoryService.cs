﻿using MyPersonalWebsite.ClassLib.Data.Entities;
using MyPersonalWebsite.ClassLib.Models;
using MyPersonalWebsite.ClassLib.Models.Category;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPersonalWebsite.ClassLib.Services.IServices
{
    public interface ICategoryService
    {
        Task<List<Category>> GetAllCategoriesAsync();

        Task AddCategoryAsync(AddCategoryModel category);

        Task DeleteCategoryAsync(int id);

        Task UpdateCategoryAsync(EditCategoryModel obj);

        Task<EditCategoryModel> GetCategoryByIdAsync(int id);

        Task<IEnumerable<CategoryViewModel>> GetCategoryOptions();

        Task<Category> GetCategoryDeleteByIdAsync(int id);
    }
}
