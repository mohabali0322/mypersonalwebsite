﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MyPersonalWebsite.ClassLib.Data.Entities;
using MyPersonalWebsite.ClassLib.Models.NewsCategory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPersonalWebsite.ClassLib.Services.IServices
{
    public interface INewsCategoryService
    {
        Task<List<NewsCategory>> GetAllNewsCategories();

        Task AddNewsCategory(AddNewsCategoryModel addNewsModel);

        Task DeleteNewsCategory(int id);

        Task UpdateNewsCategory(EditNewsCategoryModel editNewsModel);

        Task<EditNewsCategoryModel> GetNewsCategoryByIdAsync(int id);

        Task<NewsCategory> GetNewsCategoryById(int id);

        Task<List<SelectListItem>> GetCategoryOptions();

        Task<List<News>> GetNewsforCategory(int id);

        Task<IEnumerable<NewsCategoryViewModel>> GetOtherCategoryOptions();
    }
}
