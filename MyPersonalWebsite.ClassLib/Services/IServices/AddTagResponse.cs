﻿namespace MyPersonalWebsite.ClassLib.Services.IServices
{
    public class AddTagResponse
    {
        public bool Success { get; set; }
        public string Message { get; set; }
    }
}