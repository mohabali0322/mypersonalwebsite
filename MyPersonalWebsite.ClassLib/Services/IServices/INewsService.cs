﻿using MyPersonalWebsite.ClassLib.Data.Entities;
using MyPersonalWebsite.ClassLib.Models.News;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPersonalWebsite.ClassLib.Services.IServices
{
    public interface INewsService
    {
        Task<List<News>> GetAllNews();

        Task AddNews(AddNewsModel addNewsModel);

        Task DeleteNews(int id);

        Task UpdateNews(EditNewsModel editNewsModel);

        Task<EditNewsModel> GetNewsByIdAsync(int id);

        Task<News> GetNewsById(int id);
    }
}
