﻿using MyPersonalWebsite.ClassLib.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPersonalWebsite.ClassLib.Services.IServices
{
    public interface ICommentService 
    {
      Task<List<Comment>> GetComments(int id);
        
    }
}
