﻿using MyPersonalWebsite.ClassLib.Data.Entities;
using MyPersonalWebsite.ClassLib.Models.Author;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPersonalWebsite.ClassLib.Services.IServices
{
    public interface IAuthorService
    {
        Task <List<Author>> GetAllAuthorsAsync();

        Task AddAuthorAsync(AddAuthorModel obj);

        Task DeleteAuthorAsync(int id);

        Task UpdateAuthorAsync(EditAuthorModel obj);

        Task<EditAuthorModel> GetAuthorByIdAsync(int id);

        Task<List<Microsoft.AspNetCore.Mvc.Rendering.SelectListItem>> GetAuthorOptions();

        Task<Author> GetAuthorDeleteByIdAsync(int id);


    }
}
