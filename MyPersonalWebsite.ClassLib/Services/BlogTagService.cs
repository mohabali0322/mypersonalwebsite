﻿using Microsoft.EntityFrameworkCore;
using MyPersonalWebsite.ClassLib.Data;
using MyPersonalWebsite.ClassLib.Data.Entities;
using MyPersonalWebsite.ClassLib.Models;
using MyPersonalWebsite.ClassLib.Services.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPersonalWebsite.ClassLib.Services
{
    public class BlogTagService : IBlogTagService

    {
        private MyDbContext _db;

        private readonly IBlogService _service;


        public BlogTagService(MyDbContext db, IBlogService service)
        {
            _db = db;
            _service = service;
        }

        public async Task<AddTagResponse> AddTag(AddTagModel addTagModel)
        {
            var blog = await GetBlogById(addTagModel.BlogsId);
            var existingTag = _db.Tags.SingleOrDefault(x => x.Name == addTagModel.Name);
            
            if (existingTag != null)
            {
                if (!blog.Tags.Contains(existingTag))
                {
                    blog.Tags.Add(existingTag);
                    await _db.SaveChangesAsync();
                    return new AddTagResponse()
                    {
                        Success = true,
                        Message = "Tag Added."
                    };
                }
                return new AddTagResponse()
                {
                    Success = false,
                    Message = "Tag already exists."
                };
            }
            else
            {
                var newTag = new Tag
                {
                    Name = addTagModel.Name,
                };

                _db.Tags.Add(newTag);
                blog.Tags.Add(newTag);
                await _db.SaveChangesAsync();

                return new AddTagResponse()
                {
                    Success = true,
                    Message = "Tag Added."
                };
            }

        }

        private async Task<Blog> GetBlogById(int id)
        {
            return await _db.Blogs.Include(y => y.Tags).SingleAsync(x => x.Id == id);
        }

        public async Task DeleteTag(DeleteTagModel model)
        {
            var blog = await GetBlogById(model.BlogId);
            var tag = blog.Tags.Single(x => x.Id == model.TagId);

            blog.Tags.Remove(tag);
            await _db.SaveChangesAsync();
        }

        public async Task<DeleteTagModel> GetDeleteModel(DeleteTagModel model)
        {
            var blog = await GetBlogById(model.BlogId);
            var tag = blog.Tags.Single(x => x.Id == model.TagId);

            model.Description = $"Are you sure you want to remove {tag.Name} from {blog.Name}?";

            return model;
        }

        public async Task<List<Tag>> GetTagsForBlog(int id)
        {
            return await _db.Blogs.Include(x => x.Tags).Where(x => x.Id == id).SelectMany(x => x.Tags).ToListAsync();
        }
    }
}

