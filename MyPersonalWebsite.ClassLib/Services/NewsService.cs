﻿using Microsoft.EntityFrameworkCore;
using MyPersonalWebsite.ClassLib.Data;
using MyPersonalWebsite.ClassLib.Data.Entities;
using MyPersonalWebsite.ClassLib.Models.News;
using MyPersonalWebsite.ClassLib.Models.NewsCategory;
using MyPersonalWebsite.ClassLib.Services.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPersonalWebsite.ClassLib.Services
{
    public class NewsService : INewsService
    {
        private MyDbContext _db;

        public NewsService(MyDbContext db)
        {
            _db = db;
        }
        public async Task AddNews(AddNewsModel addNewsModel)
        {
            var dbNews = new News
            {
                Title = addNewsModel.Title,
                PublishOn = addNewsModel.PublishOn,
                Body = addNewsModel.Body,
            };

            _db.News.Add(dbNews);
            await _db.SaveChangesAsync();
        }

        public async Task DeleteNews(int id)
        {
            News deleteNews = _db.News.FirstOrDefault(u => u.Id == id);
            if (deleteNews != null)
            {
                _db.Remove(deleteNews);
                await _db.SaveChangesAsync();
            }
        }

        public async Task<List<News>> GetAllNews()
        {
            var newsList = await _db.News.ToListAsync();

            return newsList;
        }

        public async Task<News> GetNewsById(int id)
        {
            News news = await _db.News.SingleAsync(u => u.Id == id);

            return news;
        }

        public async Task<EditNewsModel> GetNewsByIdAsync(int id)
        {
            var newsById = await _db.News.Include(x=>x.OtherCategories).SingleAsync(u => u.Id == id);
            var allnewsCategories = await _db.NewsCategories.ToListAsync();

            var newsCategoryModel = new List<NewsCategoryViewModel>();
            foreach (var category in allnewsCategories)
            {
                newsCategoryModel.Add(new NewsCategoryViewModel
                {
                    Id = category.Id,
                    Name = category.Name,
                    IsSelected = newsById.OtherCategories?.Any(c => c.Id == category.Id) ?? false
                });                
            }

            var editNews = new EditNewsModel
            {
                Id = newsById.Id,
                Title = newsById.Title,
                Body = newsById.Body,
                PublishOn = newsById.PublishOn,
                PrimaryCategoryId = newsById.PrimaryCategoryId,
                AuthorId = newsById.AuthorId,
                OtherCategories = newsCategoryModel
            };

            return editNews;
        }


        public async Task UpdateNews(EditNewsModel editNewsModel)
        {
            var news = await _db.News.Include(x => x.OtherCategories).SingleAsync(x => x.Id == editNewsModel.Id);
            var selectedNewsCategoryIds = editNewsModel.OtherCategories?.Where(x => x.IsSelected).Select(x => x.Id).ToList();
           

            news.Title = editNewsModel.Title;
            news.Body = editNewsModel.Body;
            news.PublishOn = editNewsModel.PublishOn;
            news.PrimaryCategoryId = editNewsModel.PrimaryCategoryId;
            news.AuthorId = editNewsModel.AuthorId;

            List<NewsCategory> newsCategoryToRemove = news.OtherCategories.Where(y => !selectedNewsCategoryIds.Contains(y.Id)).ToList();
            List<int> newsCategoryToAdd = selectedNewsCategoryIds.Where(i => !news.OtherCategories.Select(newsCategory => newsCategory.Id).Contains(i)).ToList();

            foreach (var newsCategoryid in newsCategoryToAdd)
            {
                var newsCategory = await _db.NewsCategories.SingleAsync(u => u.Id == newsCategoryid);
                news.OtherCategories.Add(newsCategory);
            }

            foreach (var newsCategory in newsCategoryToRemove)
            {
                news.OtherCategories.Remove(newsCategory);
            }

            _db.News.Update(news);
            await _db.SaveChangesAsync();
        }
    }
}

