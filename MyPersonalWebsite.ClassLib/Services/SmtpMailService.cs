﻿using Microsoft.Extensions.Options;
using MyPersonalWebsite.ClassLib.Models;
using MyPersonalWebsite.ClassLib.Services.IServices;
using MyPersonalWebsite.ClassLib.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace MyPersonalWebsite.ClassLib.Services
{
    public class SmtpMailService : IMailService
    {
        private readonly SmtpSettings _settings;

        public SmtpMailService(IOptions<SmtpSettings> settings)
        {
            _settings = settings.Value;

        }

        public async Task Send(MailMessageModel mailMessageModel)
        {
            using (var smtp = new SmtpClient())
            {
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential(_settings.UserName, _settings.Password);
                smtp.Host = _settings.Host;
                smtp.Port = _settings.Port;
                smtp.EnableSsl = _settings.EnableSsl;
              

                var msg = new MailMessage();
                {
                    msg.From = new MailAddress(_settings.FromEmail);
                    msg.To.Add(mailMessageModel.ToAddress);
                    msg.Subject = mailMessageModel.Subject;
                    msg.Body = mailMessageModel.Body;
                    msg.IsBodyHtml = mailMessageModel.IsHtml;                }

                await smtp.SendMailAsync(msg);
            }
        }
    }
}
