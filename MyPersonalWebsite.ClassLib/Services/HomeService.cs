﻿using MyPersonalWebsite.ClassLib.Models;
using MyPersonalWebsite.ClassLib.Services.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPersonalWebsite.ClassLib.Services
{
   public class HomeService : IHomeService
    {
        public async Task<HomePageModel> HomePageDetails()
        {

            var homePageDetails = new HomePageModel
            {
                HeroImageUrl = "../Images/admin.jpg",
                HomePageText = "Welcome Admin"
            };

            return homePageDetails;
        }
    }
}
