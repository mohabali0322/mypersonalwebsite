﻿using Microsoft.EntityFrameworkCore;
using MyPersonalWebsite.ClassLib.Data;
using MyPersonalWebsite.ClassLib.Data.Entities;
using MyPersonalWebsite.ClassLib.Models;
using MyPersonalWebsite.ClassLib.Models.Blog;
using MyPersonalWebsite.ClassLib.Services.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;


namespace MyPersonalWebsite.ClassLib.Services
{
    public class BlogService : IBlogService

    {
        private MyDbContext _db;

        public BlogService(MyDbContext db)
        {
            _db = db;
        }

        public async Task<List<Blog>> GetAllBlogs()
        {
            var allBlogs = await _db.Blogs.OrderByDescending(x => x.CreatedOn).ToListAsync();
            return (allBlogs);
        }
        public async Task AddBlog(AddBlogModel addBlogModel)
        {
            var blog = new Blog
            {
                Name = addBlogModel.Name,
                CreatedOn = addBlogModel.CreatedOn,
                Body = addBlogModel.Body,
            };
            _db.Add(blog);
            await _db.SaveChangesAsync();
        }

        public async Task DeleteBlog(int id)
        {
            var obj = await _db.Blogs.FirstOrDefaultAsync(u => u.Id == id);
            if (obj != null)
            {
                _db.Remove(obj);
               await _db.SaveChangesAsync();
            }
        }

        public async Task UpdateBlog(EditBlogModel obj)
        {
            var blog = await _db.Blogs.Include(x=>x.Categories).SingleAsync(u => u.Id == obj.Id);
            var selectedCategoryIds = obj.Categories?.Where(x => x.IsSelected).Select(x => x.Id).ToList();

            blog.Name = obj.Name;
            blog.CreatedOn = obj.CreatedOn;
            blog.Body = obj.Body;
            blog.AuthorId = obj.AuthorId;
            
            //AssignCategories(blog, selectedCategoryIds);
            List<Category> categoryToRemove =blog.Categories.Where(x=>!selectedCategoryIds.Contains(x.Id)).ToList();
            List<int> categoriesToAdd = selectedCategoryIds.Where(i=>!blog.Categories.Select(category => category.Id).Contains(i)).ToList();

            foreach (var categoryid in categoriesToAdd)
            {
                var category = await _db.Categories.SingleAsync(u => u.Id == categoryid);
                blog.Categories.Add(category);
            }

            foreach(var category in categoryToRemove)
            {
                blog.Categories.Remove(category);
            }
            
           
            _db.Blogs.Update(blog);
            await _db.SaveChangesAsync();
        }

        private void AssignCategories(Blog blog, List<int>? categories)
        {
            if (categories == null || !categories.Any())
            {
                blog.Categories = new List<Category>();
                return;
            }
        }

        public async Task<EditBlogModel> GetBlogByIdAsync(int id)
        {
            var blog = await _db.Blogs.Include(x => x.Categories).SingleAsync(x => x.Id == id);
            var allCategories = await _db.Categories.ToListAsync();

            var categoryModel = new List<CategoryViewModel>();
            foreach (var category in allCategories)
            {
                categoryModel.Add(new CategoryViewModel
                {
                    Id = category.Id,
                    Name = category.CategoryName,
                    IsSelected = blog.Categories.Any(c => c.Id == category.Id),
                });
            }

            return new EditBlogModel
            {
                Id = blog.Id,
                Name = blog.Name,
                CreatedOn = blog.CreatedOn,
                Body = blog.Body,
                AuthorId = blog.AuthorId,
                Categories = categoryModel
            };
        }

        public async Task<Blog> GetBlogDeleteByIdAsync(int id)
        {
            return await _db.Blogs.FirstOrDefaultAsync(u => u.Id == id);
        }
    }
}
