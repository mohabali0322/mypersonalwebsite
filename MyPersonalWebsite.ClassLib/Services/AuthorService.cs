﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MyPersonalWebsite.ClassLib.Data;
using MyPersonalWebsite.ClassLib.Data.Entities;
using MyPersonalWebsite.ClassLib.Models.Author;
using MyPersonalWebsite.ClassLib.Services.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPersonalWebsite.ClassLib.Services
{
    public class AuthorService : IAuthorService
    {
        private MyDbContext _db;

        public AuthorService(MyDbContext db)
        {
            _db = db;
        }

        public async Task<List<Author>> GetAllAuthorsAsync()
        {
            var authorList = await _db.Authors.ToListAsync();

            return authorList;
        }

        public async Task AddAuthorAsync(AddAuthorModel model)
        {
            var author = new Author
            {
                FirstName = model.FirstName,
                LastName = model.LastName,
                Email = model.Email,
            };

            _db.Add(author);
            await _db.SaveChangesAsync();
        }

        public async Task UpdateAuthorAsync(EditAuthorModel model)
        {
            var author = await _db.Authors.SingleAsync(x => x.AuthorId == model.Id);

            author.FirstName = model.FirstName;
            author.LastName = model.LastName;
            author.Email = model.Email;

            _db.Authors.Update(author);
            await _db.SaveChangesAsync();
        }

        public async Task DeleteAuthorAsync(int id)
        {
            var author = await _db.Authors.FirstOrDefaultAsync(u => u.AuthorId == id);
            if (author != null)
            {
                _db.Remove(author);
               await _db.SaveChangesAsync();
            }
        }

        public async Task<EditAuthorModel> GetAuthorByIdAsync(int id)
        {
            var author = await _db.Authors.FirstOrDefaultAsync(u => u.AuthorId == id);

            var authorModel = new EditAuthorModel
            {
                Id = author.AuthorId,
                FirstName = author.FirstName,
                LastName = author.LastName,
                Email = author.Email,
            };

            return authorModel;
        }

        public async Task<List<SelectListItem>> GetAuthorOptions()
        {
            var authors = await GetAllAuthorsAsync();

            return  (from author in authors
                              select new SelectListItem()
                              {
                                  Text = author.FirstName,
                                  Value = author.AuthorId.ToString(),
                              }).ToList();
        }


        public async Task<Author> GetAuthorDeleteByIdAsync(int id)
        {
            return await _db.Authors.FirstOrDefaultAsync(u => u.AuthorId == id);
        }
    }
}




