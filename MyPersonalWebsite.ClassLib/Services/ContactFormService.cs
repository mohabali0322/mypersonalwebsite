﻿using MyPersonalWebsite.ClassLib.Models;
using MyPersonalWebsite.ClassLib.Services.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPersonalWebsite.ClassLib.Services
{
    public class ContactFormService : IContactFormService
    {
        public ContactFormViewModel GetContactFormById(int id)
        {
            var forms = new List<ContactFormViewModel>();
            forms.Add(new ContactFormViewModel { Id = 1, Name = "Mohab", Email = "mohab.ali@thought.co.uk", Message = "Hello There 1" });
            forms.Add(new ContactFormViewModel { Id = 2, Name = "Will", Email = "info@thought.co.uk", Message = "Hello There 2" });
            forms.Add(new ContactFormViewModel { Id = 3, Name = "Jamie", Email = "info@thought.co.uk", Message = "Hello There 3" });
            forms.Add(new ContactFormViewModel { Id = 4, Name = "Robbie", Email = "info@thought.co.uk", Message = "Hello There 4" });




            return forms.First(x => x.Id == id);
        }
    }
}
