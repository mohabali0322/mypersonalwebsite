﻿namespace MyPersonalWebsite.ClassLib.Services
{
    public enum SortOrder
    {
        DateAsc,
        DateDesc
    }
}