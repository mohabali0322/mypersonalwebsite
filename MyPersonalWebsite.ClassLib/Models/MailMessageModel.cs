﻿namespace MyPersonalWebsite.ClassLib.Models
{
    public class MailMessageModel
    {
        public string FromAddress { get; set; }
        public string ToAddress { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public bool IsHtml { get; set; }
    }
}
