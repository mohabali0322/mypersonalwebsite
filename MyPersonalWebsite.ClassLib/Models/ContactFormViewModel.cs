﻿using System.ComponentModel.DataAnnotations;

namespace MyPersonalWebsite.ClassLib.Models
{
    public class ContactFormViewModel
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(100,MinimumLength =5)]
        public string Name { get; set; }

        [Required]
        [EmailAddress(ErrorMessage = "Please enter a valid email")]
        public string Email { get; set; }

        [Required]
        [StringLength(500, ErrorMessage ="Message exceeds limit 500 Characters")]
        public string Message { get; set; }
    }
}
