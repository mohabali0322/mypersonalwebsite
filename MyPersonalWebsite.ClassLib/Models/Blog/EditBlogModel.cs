﻿namespace MyPersonalWebsite.ClassLib.Models
{
    public class EditBlogModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Body { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? AuthorId { get; set; }

        public List<CategoryViewModel>? Categories { get; set; }
    }
}