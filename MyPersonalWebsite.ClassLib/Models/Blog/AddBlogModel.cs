﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPersonalWebsite.ClassLib.Models.Blog
{
    public class AddBlogModel
    {
        public string Name { get; set; }

        [DisplayName("Created On")]
        public DateTime CreatedOn { get; set; }

        public string Body { get; set; }

    }
}
