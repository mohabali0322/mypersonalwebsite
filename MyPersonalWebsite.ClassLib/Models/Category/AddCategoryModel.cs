﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPersonalWebsite.ClassLib.Models.Category
{
    public class AddCategoryModel
    {
        public string Name { get; set; }
    }
}
