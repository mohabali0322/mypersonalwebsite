﻿namespace MyPersonalWebsite.ClassLib.Models
{
    public class HomePageModel
    {

        public string HeroImageUrl { get; set; }

        public string HomePageText { get; set; }
    }
}
