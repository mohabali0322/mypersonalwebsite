﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPersonalWebsite.ClassLib.Models
{
    public class AddCommentModel 
    {
        public string Name { get; set; }
        public string Body { get; set; }
        public int BlogId { get; set; }
    }
}
