﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPersonalWebsite.ClassLib.Models.NewsCategory
{
    public class AddNewsCategoryModel
    {
        public string Name { get; set; }
    }
}
