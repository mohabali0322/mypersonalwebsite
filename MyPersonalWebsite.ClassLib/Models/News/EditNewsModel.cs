﻿using MyPersonalWebsite.ClassLib.Models.NewsCategory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPersonalWebsite.ClassLib.Models.News
{
    public class EditNewsModel
    {
        public int Id { get; set; }
        public string Title { get; set; }

        public string Body { get; set; }

        public DateTime PublishOn { get; set; }
        public int? AuthorId { get; set; }
        public int? PrimaryCategoryId { get; set; }
        public List<NewsCategoryViewModel>? OtherCategories { get; set; }
    }
}
