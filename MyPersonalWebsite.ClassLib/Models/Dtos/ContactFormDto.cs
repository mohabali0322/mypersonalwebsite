﻿using System.ComponentModel.DataAnnotations;

namespace MyPersonalWebsite.ClassLib.Models.Dtos
{
    public class ContactFormDto
    {
        
        public int Id { get; set; }

        
        public string Name { get; set; }

     
        public string Email { get; set; }

       
        public string Message { get; set; }
    }
}
