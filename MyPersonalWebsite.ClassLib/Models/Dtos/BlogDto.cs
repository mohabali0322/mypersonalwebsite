﻿namespace MyPersonalWebsite.ClassLib.Models.Dtos
{
    public class BlogDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime BlogTime { get; set; }
        public string Message { get; set; }
        public string Image { get; set; }
    }
}




