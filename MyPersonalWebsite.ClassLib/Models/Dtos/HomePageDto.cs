﻿namespace MyPersonalWebsite.ClassLib.Models.Dtos
{
    public class HomePageDto
    {

        public string HeroImageUrl { get; set; }

        public string HomePageText { get; set; }
    }
}
