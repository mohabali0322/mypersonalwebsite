﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPersonalWebsite.ClassLib.Models
{
    public class DeleteTagModel
    {
        public string? Description { get; set; }

        public int BlogId { get; set; }
        public int TagId { get; set; }
    }
}
