﻿namespace MyPersonalWebsite.ClassLib.Models
{
    public class BlogModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime BlogTime { get; set; }
        public string Message { get; set; }
        public string Image { get; set; }
    }
}




