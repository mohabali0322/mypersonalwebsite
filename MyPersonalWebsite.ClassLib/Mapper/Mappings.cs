﻿using AutoMapper;
using MyPersonalWebsite.ClassLib.Models;
using MyPersonalWebsite.ClassLib.Models.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPersonalWebsite.ClassLib.Mapper
{
    public class Mappings : Profile 
    {
        public Mappings()
        {
            CreateMap<BlogModel, BlogDto>().ReverseMap();
            CreateMap<ContactFormViewModel, ContactFormDto>().ReverseMap();
            CreateMap<HomePageModel, HomePageDto>().ReverseMap();


        }
    }
}
