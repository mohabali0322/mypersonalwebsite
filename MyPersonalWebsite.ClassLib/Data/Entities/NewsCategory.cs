﻿namespace MyPersonalWebsite.ClassLib.Data.Entities
{
    public class NewsCategory
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public ICollection<News>? PrimaryCategoryNews { get; set; }
        
        public ICollection<News>? OtherCategoryNews { get; set; }
    }
}