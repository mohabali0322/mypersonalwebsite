﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPersonalWebsite.ClassLib.Data.Entities
{
    public class Blog
    {
        public int Id { get; set; }
        [StringLength(255)]
        public string Name { get; set; }

        [StringLength(1000)]
        public string Body { get; set; }

        [DisplayName("Created On")]
        public DateTime CreatedOn { get; set; }
        public DateTime UpdateOn { get; set; }

        public List<Comment> Comments { get; } = new ();

        public ICollection<Tag> Tags { get; set; }

        public int? AuthorId { get; set; }
        public Author? Author { get; set; }

        public ICollection<Category> Categories { get; set; }
    }
}
