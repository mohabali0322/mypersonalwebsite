﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPersonalWebsite.ClassLib.Data.Entities
{
    public class Category
    {
        public int Id { get; set; }

        [Display(Name ="Category Name")]
        [StringLength(255)]
        public string CategoryName { get; set; }

        public ICollection<Blog>? Blogs { get; set; }
    }
}
