﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPersonalWebsite.ClassLib.Data.Entities
{
    public class News
    {
        public int Id { get; set; }

        [StringLength(200)]
        public string Title { get; set; }

        public string Body { get; set; }

        [Display(Name="Published On")]
        public DateTime PublishOn { get; set; }

        public int? PrimaryCategoryId { get; set; }

        public NewsCategory? PrimaryCategory { get; set; } //one-to-many

        public ICollection<NewsCategory>? OtherCategories { get; set; } //many-to-many

        public int? AuthorId { get; set; }
        public Author? Author { get; set; }


    }
}
