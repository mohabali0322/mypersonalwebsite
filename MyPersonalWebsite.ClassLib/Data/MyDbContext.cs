﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using MyPersonalWebsite.ClassLib.Data.Entities;
using MyPersonalWebsite.ClassLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyPersonalWebsite.ClassLib.Data
{
    public class MyDbContext : IdentityDbContext
    {
        public DbSet<Blog> Blogs { get; set; }

        public DbSet<Comment> Comments { get; set; }

        public DbSet<Tag> Tags { get; set; }

        public DbSet<Author> Authors { get; set; }

        public DbSet<Category> Categories { get; set; }

        public DbSet<News> News { get; set; }

        public DbSet<NewsCategory> NewsCategories { get; set; }

        public MyDbContext(DbContextOptions<MyDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Blog>().ToTable("Blog")
                .HasData(new Blog() { Id=1, Name = "Blog 1", Body="Some Body", CreatedOn = new DateTime(2000,1,1), UpdateOn = new DateTime(2000,1,1), AuthorId =2}, new Blog { Id=2, Name = "Blog 2", Body = "Some Body 2", CreatedOn = new DateTime(2000,1,1), UpdateOn = new DateTime(2000,1,1), AuthorId = 1 });
            
            modelBuilder.Entity<News>()
                .HasOne(x=>x.PrimaryCategory)
                .WithMany(x=>x.PrimaryCategoryNews)
                .HasForeignKey(x=>x.PrimaryCategoryId);

            modelBuilder.Entity<News>().HasMany(x => x.OtherCategories)
                .WithMany(x => x.OtherCategoryNews);
            
            base.OnModelCreating(modelBuilder);

        }
    }

    public static class InitData
    {
        public static void Initialize(MyDbContext context)
        {
            context.Database.EnsureCreated();
            if (!context.Blogs.Any())
            {
                var blog = new Blog
                {
                    Name = "My First Blog",
                    Body = "Body",
                    CreatedOn = DateTime.Now,
                    UpdateOn = DateTime.Now,
                    AuthorId = 1,
                    
                };

                context.Blogs.Add(blog);

                context.SaveChanges();
            }

        }
           
    }
}
